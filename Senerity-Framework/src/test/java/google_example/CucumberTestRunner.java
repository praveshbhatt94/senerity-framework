package google_example;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

import java.io.File;
import java.io.IOException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import google_example.util.dockerrunner.StartDocker;
import google_example.util.dockerrunner.StopDocker;


@RunWith(CucumberWithSerenity.class)
@CucumberOptions(plugin = {"pretty"},features = "classpath:features")
public class CucumberTestRunner {
//	CucumberTestSuite
	
	@BeforeClass
	public  static void startDockerScale() throws IOException, InterruptedException
	{
		File fi =new File("output.txt");
		if(fi.delete())
		{
			System.out.println("file deleted successfully");
		}
		StartDocker s=new StartDocker();
		s.startFile();
	}
	
	@AfterClass
	public static void stopDockerDeleteFile() throws IOException, InterruptedException
	{
		StopDocker d=new StopDocker();
		d.stopFile();
		
		
	}
	
	
}

